<?php

class Customer
{
    private $conn;
    private $table = 'customer';
    public $ID;
    public $FirstName;
    public $Lastname;
    public $Username;
    public $Password;
    public $DateOfBirth;
    public $CreatedAt;
    public $UpdatedAt;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    //GET Customer
    public function read()
    {
        $query = 'SELECT * FROM ' . $this->table;
        $statement = $this->conn->prepare($query);
        $statement->execute();
        return $statement;
    }

    //create Api for  Customer
    public function create()
    {

        $query = 'INSERT INTO ' . $this->table .'
        SET FirstName = :FirstName,
        Lastname= :Lastname,
        Username = :Username,
        Password = :Password,
        DateofBirth = :DateofBirth,
        CreatedAt = :CreatedAt,
        UpdatedAt = :UpdatedAt';

        $statement = $this->conn->prepare($query);

        $this->FirstName = htmlspecialchars(strip_tags($this->FirstName));
        $this->LastName = htmlspecialchars(strip_tags($this->Lastname));
        $this->UserName = htmlspecialchars(strip_tags($this->Username));
        $this->Password = htmlspecialchars(strip_tags(md5($this->Password)));
        $this->DateOfBirth = $this->DateOfBirth;
        $this->CreatedAt =  date('Y-m-d H:i:s');
        $this->UpdatedAt =  date('Y-m-d H:i:s');

        $statement->bindParam(':FirstName', $this->FirstName);
        $statement->bindParam(':Lastname', $this->LastName);
        $statement->bindParam(':Username', $this->UserName);
        $statement->bindParam(':Password', $this->Password);
        $statement->bindParam(':DateofBirth', $this->DateOfBirth);
        $statement->bindParam(':CreatedAt', $this->CreatedAt);
        $statement->bindParam(':UpdatedAt', $this->UpdatedAt);
        if ($statement->execute()) {
            return true;
        }
        print_r('Error %s. \n'.$statement->error);
        return false;

    }

    public function update()
    {

        $query = 'UPDATE ' . $this->table .'
        SET FirstName  = :FirstName,
        Lastname       = :Lastname,
        Username       = :Username,
        Password       = :Password,
        DateofBirth    = :DateofBirth,
        UpdatedAt      = :UpdatedAt 
        WHERE ID =' .$this->ID;


        $statement = $this->conn->prepare($query);
      //  $this->ID = htmlspecialchars(strip_tags($this->ID));
        $this->FirstName = htmlspecialchars(strip_tags($this->FirstName));
        $this->Lastname = htmlspecialchars(strip_tags($this->Lastname));
        $this->UserName = htmlspecialchars(strip_tags($this->Username));
        $this->Password = htmlspecialchars(strip_tags(md5($this->Password)));
        $this->DateOfBirth = htmlspecialchars(strip_tags($this->DateOfBirth));
        $this->UpdatedAt =  date('Y-m-d H:i:s');
     //   $statement->bindParam(':ID', $this->ID);
        $statement->bindParam(':FirstName', $this->FirstName);
        $statement->bindParam(':Lastname', $this->Lastname);
        $statement->bindParam(':Username', $this->UserName);
        $statement->bindParam(':Password', $this->Password);
        $statement->bindParam(':DateofBirth', $this->DateofBirth);
        $statement->bindParam(':UpdatedAt', $this->UpdatedAt);
        if ($statement->execute()) {
            return true;
        }
        print_r('Error %s. \n'.$statement->error);
        return false;

    }


}