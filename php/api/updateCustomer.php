<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');
/*header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: origin, Content-Type, accept');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods:POST');
header('Content-Type: application/json');
header('Access-Control-Allow-Headers:Access-Control-Allow-Headers,Access-Control-Allow-Methods,Authorization,X-Requested-With');*/
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");
require_once 'autoload.php';
require_once '../class/Customer.php';
require_once '../config/Database.php';

$database = new Database();
$db = $database->connect();
//$listRegisterEventArray = [];
$customerCreate = new Customer($db);
$data = json_decode(file_get_contents("php://input"));


$errorMessage = [];
$error['error']['message'] = [];

if (empty($data->ID)) {
    $errorMessage[] = "ID field is mandatory";

}

if (empty($data->FirstName)) {
    $errorMessage[] = "FirstName field is mandatory";

} else {
    if (!preg_match("/^[a-zA-Z0-9_ ]*$/", $data->FirstName)) {
        $errorMessage[] = "FirstName must only contain letters!";
    }

    if (strlen($data->FirstName) > 50) {
        $errorMessage[] = "Input is too long, maximum is 50 characters.";
    }
}

if (empty($data->Lastname)) {
    $errorMessage[] = "Lastname field is mandatory";

} else {
    if (!preg_match("/^[a-zA-Z\s]+$/", $data->Lastname)) {
        $errorMessage[] = "Lastname must only contain letters!";
    }

    if (strlen($data->Lastname) > 50) {
        $errorMessage[] = "Input is too long, maximum is 50 characters.";
    }
}

if (empty($data->Username)) {
    $errorMessage[] = "Username field is mandatory";

} else {
    if (!preg_match("/^[a-zA-Z\s]+$/", $data->Username)) {
        $errorMessage[] = "Username must only contain letters!";
    }

    if (strlen($data->Lastname) > 50) {
        $errorMessage[] = "Input is too long, maximum is 50 characters.";
    }
}


if (empty($data->DateofBirth)) {
    $errorMessage[] = "DateofBirth field is mandatory.";

}


array_push($error['error']['message'], $errorMessage);

if (count($errorMessage) > 0) {
    echo json_encode($error);
} else {

    $customerCreate->ID = $data->ID;
    $customerCreate->FirstName = $data->FirstName;
    $customerCreate->Lastname = $data->Lastname;
    $customerCreate->Username = $data->Username;
    $customerCreate->Password = $data->Password;
    $customerCreate->DateofBirth = $data->DateofBirth;


    if ($customerCreate->update()) {
        echo json_encode(
            array('message' => 'Customer updated!')
        );
    } else {
        echo json_encode(
            array('message' => 'Customer  not updated!')
        );
    }
}

