<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once 'autoload.php';
require_once '../config/Database.php';
require_once '../class/Customer.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$database = new Database();
$db = $database->connect();
$listCustomerArray = [];
$listCustomerEvent = new Customer($db);
$result = $listCustomerEvent->read();
$number = $result->rowCount();
$listCustomerArrayList['response'] = [];
$listCustomerArrayList['response']['code'] = [];
$listCustomerArrayList['response']['data'] = [];

if ($number > 0) {
    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);
        $listCustomerEventVal = array(
            'ID' => $ID,
            'FirstName' => $FirstName,
            'Lastname' => $Lastname,
            'Username' => $Username,
            'Password' => $Password,
            'DateofBirth' => $DateofBirth,
        );
        $listRegisterEventArrayList['response']['code'] = 200;
        $listCustomerArrayList['response']['message'] = 'customer successfully!';
        array_push($listCustomerArrayList['response']['data'], $listCustomerEventVal);
    }

} else {
    $listCustomerArrayList['response']['code'] = 404;
    $listCustomerArrayList['response']['message'] = 'data not found';
    $listCustomerArrayList['response']['data'] = [];
}

echo json_encode($listCustomerArrayList);
