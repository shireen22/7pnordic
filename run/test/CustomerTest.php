<?php

require './vendor/autoload.php';
require_once './php/config/Database.php';
require_once './php/class/Customer.php';

use PHPUnit\Framework\TestCase;

class CustomerTest extends TestCase
{

    public function testCreateCustomer() {

        $database = new Database();
        $db = $database->connect();
        $customer = new Customer($db);

        $customer->FirstName = 'shireen';
        $customer->Lastname = 'kolachi';
        $customer->Username = 'kolachi';
        $customer->Password = md5('userpass12');
        $customer->DateofBirth = 1990-12-22;
        $result = $customer->create();

        // Assert that the create operation was successful
        $this->assertTrue($result);
    }


    public function testUpdateCustomer() {

        $database = new Database();
        $db = $database->connect();
        $customer = new Customer($db);

        $customer->ID = 1;
        $customer->FirstName = 'shireen';
        $customer->Lastname = 'kolachi';
        $customer->Username = 'kolachi';
        $customer->Password = md5('userpass12');
        $customer->DateofBirth = '1990-12-22';
        $result = $customer->update();

        // Assert that the create operation was successful
        $this->assertTrue($result);
    }
}
