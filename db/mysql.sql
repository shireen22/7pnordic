-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.30 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for 7pnordic
CREATE DATABASE IF NOT EXISTS `7pnordic` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `7pnordic`;

-- Dumping structure for table 7pnordic.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `ID` int unsigned NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(225) NOT NULL,
  `Lastname` varchar(225) DEFAULT NULL,
  `Username` varchar(225) DEFAULT NULL,
  `Password` varchar(225) DEFAULT NULL,
  `DateofBirth` date DEFAULT NULL,
  `CreatedAt` timestamp NULL DEFAULT NULL,
  `UpdatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table 7pnordic.customer: ~26 rows (approximately)
INSERT INTO `customer` (`ID`, `FirstName`, `Lastname`, `Username`, `Password`, `DateofBirth`, `CreatedAt`, `UpdatedAt`) VALUES
	(1, 'shireen', 'kolachi', 'kolachi', '36dd6e6ccc81fb55773c3274fd20e1e4', '1990-12-22', '2024-01-21 09:34:54', '2024-01-22 18:02:36'),
	(2, 'ssssk2', 'vvv', NULL, NULL, '2024-01-21', '2024-01-21 12:51:12', '2024-01-21 12:51:14'),
	(3, 'aaaa', '', '', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '1970-01-01', NULL, NULL),
	(4, 'aaaa', '', '', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '1970-01-01', NULL, NULL),
	(5, 'aaaa', 'aaa', 'aaaaaa', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '1970-01-01', NULL, NULL),
	(6, 'aaaaa', 'ssssss', 'ssssss', '3dbe00a167653a1aaee01d93e77e730e', '1970-01-01', '2024-01-21 23:24:32', '2024-01-21 23:24:32'),
	(7, 'aaaaa', 'ssssss', 'ssssss', '3dbe00a167653a1aaee01d93e77e730e', NULL, '2024-01-21 23:25:26', '2024-01-21 23:25:26'),
	(8, 'aaaaa', 'ssssss', 'ssssss', '3dbe00a167653a1aaee01d93e77e730e', NULL, '2024-01-21 23:36:25', '2024-01-21 23:36:25'),
	(9, 'aaaaa', 'aaaa', 'aaaa', '74b87337454200d4d33f80c4663dc5e5', NULL, '2024-01-21 23:41:10', '2024-01-21 23:41:10'),
	(10, 'aaaa', 'aaaa', 'aaaa', '74b87337454200d4d33f80c4663dc5e5', NULL, '2024-01-21 23:43:11', '2024-01-21 23:43:11'),
	(11, 'sssss', 'sssss', 'ssss', '8f60c8102d29fcd525162d02eed4566b', NULL, '2024-01-21 23:49:56', '2024-01-21 23:49:56'),
	(12, 'sssss', 'sssss', 'ssss', '8f60c8102d29fcd525162d02eed4566b', NULL, '2024-01-21 23:50:09', '2024-01-21 23:50:09'),
	(13, 'aaaaaaasssss', 'aaaaaaaaa', 'aaaaaaaaamkkk', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', NULL, '2024-01-22 00:47:54', '2024-01-22 00:47:54'),
	(14, 'aaaaaaasssss', 'aaaaaaaaa', 'aaaaaaaaamkkk', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', NULL, '2024-01-22 00:48:57', '2024-01-22 00:48:57'),
	(15, 'sssss', 'ssssss', 'sssss', '16fcb1091f8a0cc70c96e2ff97fdd213', NULL, '2024-01-22 00:59:28', '2024-01-22 00:59:28'),
	(16, 'sssss', 'ssssss', 'sssss', '16fcb1091f8a0cc70c96e2ff97fdd213', NULL, '2024-01-22 00:59:48', '2024-01-22 00:59:48'),
	(17, 'sssss', 'ssssss', 'sssss', '16fcb1091f8a0cc70c96e2ff97fdd213', NULL, '2024-01-22 01:00:28', '2024-01-22 01:00:28'),
	(18, 'shireeen', 'shhhhhh', 'shhaha', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', NULL, '2024-01-22 08:52:33', '2024-01-22 08:52:33'),
	(19, 'aaaa', 'aaaa', 'aaaaa', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', NULL, '2024-01-22 09:09:45', '2024-01-22 09:09:45'),
	(20, 'hhhhoououo', 'jkjkkjjkkjjk', 'kjjkkkkkkkk', 'd8e71dbd1afc289a4b102eeadeb6f363', NULL, '2024-01-22 09:26:51', '2024-01-22 09:26:51'),
	(21, 'gjjjjjgjj', 'kkkkkkk', 'jjjjjjjjjjjjjjjjjjjjjj', 'd8e71dbd1afc289a4b102eeadeb6f363', NULL, '2024-01-22 09:27:56', '2024-01-22 09:27:56'),
	(22, 'ssssssaa', 'sssssaa', 'sssssss', 'e3ceb5881a0a1fdaad01296d7554868d', NULL, '2024-01-22 13:05:04', '2024-01-22 13:05:04'),
	(23, 'shireen', 'kolachi', 'kolachi', '36dd6e6ccc81fb55773c3274fd20e1e4', NULL, '2024-01-22 17:58:07', '2024-01-22 17:58:07'),
	(24, 'shireen', 'kolachi', 'kolachi', '36dd6e6ccc81fb55773c3274fd20e1e4', NULL, '2024-01-22 18:00:33', '2024-01-22 18:00:33'),
	(25, 'shireen', 'kolachi', 'kolachi', '36dd6e6ccc81fb55773c3274fd20e1e4', NULL, '2024-01-22 18:01:37', '2024-01-22 18:01:37'),
	(26, 'shireen', 'kolachi', 'kolachi', '36dd6e6ccc81fb55773c3274fd20e1e4', NULL, '2024-01-22 18:02:36', '2024-01-22 18:02:36');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
